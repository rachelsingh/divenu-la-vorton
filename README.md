# Divenu la Vorton

Elŝuteblaj kartoj kaj reguloj:

* **Senkolora Dosierujo** - Simplaj kartoj, senbildoj (se vi ne havas kolorpresilon)

-------------

## Pri

Divenu la Vorton estas kartoludo, simila al “Charades”. Estas multaj kartoj, kiuj havas skribitan vorton. Unu el la personoj penas priskribi la vorton, kaj la aliaj ludantoj penas diveni, kio ĝi estas.
La reguloj kaj kartoj estas elŝuteblaj cxe:
https://github.com/Esperanto-Arcade/Divenu-la-Vorton 

### Reguloj

Vi bezonas vortkartojn, paperon, skribilon, kaj amikojn por ludi.

### Prepari

1. Metu la kartaron sur la tablon. La dorsa flanko de la kartoj devas esti videbla, kaj la antaŭa parto ne-videbla.
2. Uzu paperon por registri la poentojn de ĉiu ludanto.
3. Hazarde elektu la unuan vortmajstron.

### Facila Ludo

1. La vortmajstro prenas vortkarton el la kartaro. Legu la vorton sur la karto, kaj ne diru al la aliaj ludantoj.
2. La vortmajstro priskribas la vorton, kaj diras nek la vorton nek la vortparton.
3. Ĉiuj povas diveni, ĝis iu divenas la ĝustan vorton. Tiu ĉi ludanto ricevas 1 poenton.
4. Vortmajstro forigas la karton.
5. La nova vortmajstro estas la ludanto al la dekstra.

**#3 Ekzemplo:**

Karto estas “Golfludo”.

* Vortmajstro diras: “Vi ludas tiun ĉi sur verda herbo...”
* Vortmajstro diras: “Vi uzas pilketon...”
* Ne diru “Golfludo”, “Ludo”, aŭ “Golf”.

### Laŭvole
1. Uzu tempomezurilon – la ludantoj havas nur 5 minutojn por diveni. Se ĉiuj ne ĝuste divenas, la vortmajstro gajnas la 1 poenton.
2. Uzu limojn – ĉiu ludanto nur havas 3 divenojn. Se ĉiuj ne ĝuste divenas, la vortmajstro gajnas la 1 poenton.

### Fino
1. Kiam la kartaro estas malplena, la ludo estas finita.
2. La ludanto, kiu havas la plej altaj poentoj, venkas la ludon.
3. La venkitaj ludantoj bezonas doni dolĉaĵojn al la venkanto.
